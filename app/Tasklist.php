<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasklist extends Model
{   
    protected $fillable = ['user_id', 'task', 'description', 'status'];
}