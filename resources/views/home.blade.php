@extends('layouts.app')

@section('content')
    <div class="container" ng-controller="TasklistController">
        <h1>To Do List App</h1>
        <p>Welcome to Lyle&#180;s To Do List App, built with Angularjs on top of Laravel.</p>
        <p>Please feel free to add tasks using the allocated form below. I have also provided the ability to edit the created tasks by simply clicking on the field you wish to update, and once done, the data will automatically be saved when you click out of the field. Similarly, you can update the assigned task user, by virtue of the designated select dropdown, by selecting a new option, and again, the data will be updated automatically.</p>
        <p>I will be happy to walk you through any part of this excercise. I look forward to hearing from you.</p>
        <table class="add_task">            
            <tr>
                <th>User</th>
                <th>Task</th>
                <th>Task Description</th>
                <th></th>
            </tr>                  
            <tr ng-form name="addTaskForm" ng-keyup="$event.keyCode == 13 && addTask()">
                <td>
                    <select name="user_id" ng-model="task.user_id" ng-options="user.id as user.name for user in users" required>
                        <option value="">Select User<option>
                    </select>
                    <div ng-show="addTaskForm.user_id.$invalid && (addTaskForm.user_id.$touched || submitted)">
                        <div class="invalid_msg" ng-message="required">Please select a user.</div>
                    </div>
                </td>
                <td>
                    <input type="text" name="task" ng-model="task.task" placeholder="Task" required>
                    <div ng-show="addTaskForm.task.$invalid && (addTaskForm.task.$touched || submitted)">
                        <div class="invalid_msg" ng-message="required">Please enter a task.</div>
                    </div>
                </td>
                <td>
                    <textarea name="description" rows="5" ng-model="task.description"></textarea>
                </td>
                <td>
                    <button type="button" ng-click="addTask()">Add Task</button>
                </td>
            </tr>
        </table>
        <div class="results_container">
            <div class="results_options">
                Showing <select ng-model="user.id" ng-change="loadTasklistbyUser(user.id)" ng-options="user.id as user.name for user in users">
                            <option value="">All</option>
                        </select> <span ng-if="user.id">'s</span> Tasks
            </div>
            <table>
                <thead>
                    <th>No.</th>
                    <th>User</th>
                    <th>Task</th>
                    <th>Task Description</th>
                    <th>Status</th>
                    <th>Action</th>
                </thead> 
                <tr ng-if="tasklist.length" ng-repeat="(index, task) in tasklist">
                    <td>
                        @{{ index + 1 }}
                    </td>
                    <td>
                        <select name="user_id" ng-model="task.user_id" ng-options="user.id as user.name for user in users" ng-change="updateTask(task.id, 'user_id', task.user_id, task.status)">
                            <option value="">Please select</option>
                        </select>
                        <div ng-show="edited_id == task.id && edited_field == 'user_id' && !edited_value">
                            <div class="invalid_msg">Please select a user.</div>
                        </div>
                    </td>
                    <td>
                        <div contenteditable edititem="updateTask" name="task" data-id="@{{task.id}}">
                            @{{ task.task }}                            
                        </div>
                        <div ng-show="edited_id == task.id && edited_field == 'task' && !edited_value">
                            <div class="invalid_msg">Please enter a task.</div>
                        </div>
                    </td>
                    <td>
                        <div contenteditable edititem="updateTask" name="description" data-id="@{{task.id}}">
                            @{{ task.description }}
                        </div>
                    </td>
                    <td>
                        <select name="status" ng-model="task.status" ng-options="status.id as status.status for status in statuses" ng-change="updateTask(task.id, 'status', task.status)">
                        </select>
                    </td>
                    <td>
                        <button class="delete_butt" ng-click="deleteTask(task.id, index)">Delete</button>
                    </td>
                </tr>
                <tr ng-if="!tasklist.length && user.id">
                    <td colspan="6">
                        No tasks have been added for this user.
                    </td>
                </tr>
                <tr ng-if="!tasklist.length && !user.id">
                    <td colspan="6">
                        No tasks have been added yet.
                    </td>
                </tr>
            </table> 
        </div>
    </div>
@endsection