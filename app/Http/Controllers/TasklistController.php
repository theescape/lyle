<?php

namespace App\Http\Controllers;

use App\Tasklist;
use Illuminate\Http\Request;

class TasklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Tasklist::orderBy('id', 'desc')->get();

        return response()->json([
            'tasks' => $tasks,
        ], 200);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id'     => 'required',
            'task'        => 'required|max:255'
        ]);

        $task = Tasklist::create([
            'task'        => request('task'),
            'description' => (request('description')) ? request('description') : '',
            'user_id'     => (int)request('user_id'),
            'status'      => 1,
            'created_at'  => \Carbon\Carbon::now(), 
            'updated_at'  => \Carbon\Carbon::now()
        ]);

        return response()->json([
            'tasks'    => $task,
            'message' => 'Success'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        $tasks = Tasklist::orderBy('id', 'asc')->where('user_id', $user_id)->get();

        return response()->json([
            'tasks' => $tasks,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tasklist  $tasklist
     * @return \Illuminate\Http\Response
     */
    public function edit(Tasklist $tasklist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tasklist  $tasklist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tasklist $tasklist)
    {
        $not_required_array = array('description', 'status');
        foreach($request->all() as $key => $value) {
            //if does not require validation, ignore it
            if (!in_array($key, $not_required_array)) {
                $request->validate([
                    $key => ($key === 'task') ? 'required|max:255' : 'required'
                ]);
            }
            
            $tasklist->$key = request($key);            
        }
        $tasklist->updated_at = \Carbon\Carbon::now();
        $tasklist->save();

        return response()->json([
            'message' => 'Task updated successfully!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tasklist  $tasklist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tasklist $tasklist)
    {
        $tasklist->delete();
 
        return response()->json([
            'message' => 'Task deleted successfully!'
        ], 200);
    }
}
