import 'angular';

var app = angular.module('todoAPP', []);

app.controller('TasklistController', ['$scope', '$timeout', '$http', function ($scope, $timeout, $http) {
    $scope.tasklist = [];
    $scope.submitted = false;
    $scope.task = {
        task: '',
        description: '',
        status: ''
    };

    // List items
    $scope.loadTasklist = function () {
        $http.get('/tasklist')
            .then(function success(e) {
                $scope.tasklist = e.data.tasks;
            });
    };
    $scope.loadTasklist();

    // List items By User
    $scope.loadTasklistbyUser = function (user_id) {
        //if user_id is null then set to empty to return all tasks
        $http.get('/tasklist' + ((user_id) ? '/' + user_id : ''))
            .then(function success(e) {
                $scope.tasklist = e.data.tasks;
            });
    };

    // List users
    $scope.loadUsers = function () {
        $http.get('/user')
            .then(function success(e) {
                $scope.users = e.data.users;
            });
    };
    $scope.loadUsers();

    // List statuses
    $scope.loadStatuses = function () {
        $http.get('/status')
            .then(function success(e) {
                $scope.statuses = e.data.statuses;
            });
    };
    $scope.loadStatuses();

    // Add new Task
    $scope.addTask = function () {
        $http.post('/tasklist', {
            user_id: $scope.task.user_id,
            task: $scope.task.task,
            description: $scope.task.description
        }).then(function success(e) {
            $scope.resetForm();
            $scope.tasklist.unshift(e.data.tasks);
            $timeout( function(){
                angular.element(document.querySelector('.results_container table tbody tr:first-child')).addClass('added');
            }, 10);
        }, function error(error) {
            $scope.submitted = true;
        });
    };

    $scope.edit_task = {};

    // update the given task
    $scope.updateTask = function (id, field, value) {
        $http.patch('/tasklist/' + id, {
            [field]: value
        }).then(function success(e) {
            $scope.edited_id = id;
            $scope.edited_field = field;
            $scope.edited_value = value;
        }, function error(error) {
            $scope.edited_id = id;
            $scope.edited_field = field;
            $scope.edited_value = value;
        });
    };

    // delete the given task
    $scope.deleteTask = function (id, index) {
        var conf = confirm("Do you really want to delete this task?");
        if (conf === true) {
            $http.delete('/tasklist/' + id)
                .then(function success(e) {
                    $scope.tasklist.splice(index, 1);
                });
        }
    };

    $scope.resetForm = function () {
        $scope.task.user_id = null;
        $scope.task.task = null;
        $scope.task.description = null;
        $scope.submitted = false;

        var form = $scope['addTaskForm'];
        form.$setUntouched();
        form.$setPristine();

    };
}]).directive('contenteditable', [function() {
    return {
        require: '?ngModel',
        scope: {
            edititem: "="
        },
        link: function(scope, element, attrs, ctrl) {
            element.bind('blur', function() {
                scope.$apply(function() {
                    scope.edititem(attrs.id, attrs.name, element.html());                   
                });
            });
        }
    };
}]);