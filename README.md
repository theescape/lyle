## Primary objective:
---------------------

Write a TODO List in a clean and maintainable way. It doesn't have to be beautiful, but needs to be functional.

We mostly interested in code organisation and best practice throughout the project.

Task may not need to be fully completed as long as it best shows your abilities.

We would like to get the final code submitted to this git repository in a production ready fashion - minified css/js, no debugging etc.

If fully completed, we would expect to be able to run composer install and artisan migrations and all should work from there.


## Task details:
----------------

Create a TODO List

This should be a simple 1-page application on top of Laravel.

On a page there should be:

1. h1 with a title

2. list of tasks (task name, task status ie. done/todo, and a person it belongs to)

3. Below should be a simple form to add a new task:
   Fields: task name - text, person select dropdown with at least 2 people (coming from the database) to choose from.

4. Form submissions should be handled via ajax.
   Task name and person dropdown fields should be required and error handling implemented per field.

5. We would expect database setup using migrations and Eloquent models to handle relationships between people table and tasks table.
   Relationships should be set as follows: 1 person can have multiple tasks. A task can only belong to 1 person.



## Requirements / constraints:
------------------------------

1. No css frameworks please, use scss and optional reset library

2. JS frameworks allowed

3. Implement Ajax submission with validation and error handling

4. Database persistence with relationships (person -> tasks)


** **

**Good luck!**